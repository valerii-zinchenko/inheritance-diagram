/* eslint-disable no-magic-numbers, no-shadow, max-nested-callbacks */

const assert = require('chai').assert;

const Module = require('../src/Rendering');


suite('Rendering processor (edge cases only)', () => {
	suite('Methods', () => {
		let module;
		setup(() => {
			module = new Module();
		});
		teardown(() => {
			module = null;
		});

		suite('process', () => {
			suite('exception should be thrown if NOI is not an instance of GraphNode', () => {
				[
					undefined,
					null,
					false,
					true,
					0,
					1,
					'',
					'str',
					{},
					function() {},
					[]
				].forEach(testCase => {
					test(`type: ${Object.prototype.toString.apply(testCase)}; value: ${testCase}`, async () => {
						try {
							await module.process(testCase);
							assert.fail('Exception was expected to be thrown');
						} catch (err) {
							assert.instanceOf(err, TypeError);
							assert.equal(err.message, '"noi" argument should be an instance of GraphNode');
						}
					});
				});
			});
		});
	});
});

