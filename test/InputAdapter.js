/* eslint-disable no-magic-numbers, no-shadow, max-nested-callbacks */

const assert = require('chai').assert;

const Module = require('../src/InputAdapter');


suite('InputAdapter processor (edge cases only)', () => {
	suite('Methods', () => {
		let module;
		setup(() => {
			module = new Module();
		});
		teardown(() => {
			module = null;
		});

		suite('process', () => {
			test('exception should be thrown if name of NOI is not in the map of nodes', async () => {
				try {
					await module.process('noi', {});
					assert.fail('Exception was expected to be thrown');
				} catch (err) {
					assert.instanceOf(err, Error);
					assert.equal(err.message, 'Node data for "noi" does not exist in the provided node map');
				}
			});

			suite('exception should be thrown if name of NOI is not a valid string', () => {
				[
					undefined,
					null,
					false,
					true,
					0,
					1,
					{},
					function() {},
					[],
					''
				].forEach(testCase => {
					test(`type: ${Object.prototype.toString.apply(testCase)}; value: ${testCase}`, async () => {
						try {
							await module.process(testCase, {});
							assert.fail('Exception was expected to be thrown');
						} catch (err) {
							assert.instanceOf(err, TypeError);
							assert.equal(err.message, '"name" argument is expected to be a non-empty string');
						}
					});
				});
			});

			suite('exception should be thrown if map of nodes is not an object', () => {
				[
					undefined,
					null,
					false,
					true,
					0,
					1,
					'',
					'str'
				].forEach(testCase => {
					test(`type: ${Object.prototype.toString.apply(testCase)}; value: ${testCase}`, async () => {
						try {
							await module.process('noi', testCase);
							assert.fail('Exception was expected to be thrown');
						} catch (err) {
							assert.instanceOf(err, TypeError);
							assert.equal(err.message, '"map" argument is expected to be an instance of Object class');
						}
					});
				});
			});
		});
	});
});
