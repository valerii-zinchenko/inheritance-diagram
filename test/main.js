/* eslint-disable no-magic-numbers */
const fs = require('fs');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const puppeteer = require('puppeteer');
const assert = require('chai').assert;

const diagramBuilder = require('../src/main');
const pkg = require('../package.json');

const Class = require('class-wrapper/dest/class-wrapper.amd').Class;
const GraphNode = require('../src/GraphNode');
const invalidSet = [undefined, null, false, true, 0, 1, {}, function() {}, [], ''];

const expectedDataFolder = './test/expected_data';


const toRegenerateExpectations = process.env.REGENERATE_EXPECTATIONS === 'true';
if (toRegenerateExpectations) {
	fs.readdir(expectedDataFolder, (err0, files) => {
		if (err0) throw err0;

		const path = require('path');
		for (const file of files) {
			fs.unlink(path.join(expectedDataFolder, file), err1 => {
				if (err1) throw err1;
			});
		}
	});
}


let browser;
let page;

/** Generate a screenshot from a SVG */
async function getScreenshotData(content) {
	await page.setContent(content);

	const tmp = `${pkg.directories.testOutput}/screenshot.png`;
	const clientRect = await page.evaluate(
		() => document.body.querySelector('svg')
			.getBoundingClientRect()
			.toJSON()
	);

	const width = Math.ceil(clientRect.width);
	const height = Math.ceil(clientRect.height);

	// adjust the viewport size to see the full diagram
	await page.setViewport({
		width: Math.ceil(width * 1.1),
		height: Math.ceil(height * 1.1)
	});

	// for some odd reasons the returned array buffer is creating totally weird PNG image, that is why it uses internal engine to save the image and then do compare the generated images
	await page.screenshot({
		clip: {
			x: clientRect.x,
			y: clientRect.y,
			width,
			height
		},
		path: tmp
	});

	const png = PNG.sync.read(fs.readFileSync(tmp));

	fs.unlinkSync(tmp);

	return png;
}


const TestWrapper = Class(function(title, noiName, nodeMap, options) {
	this.title = title;
	this.noiName = noiName;
	this.nodeMap = nodeMap;
	this.options = options;
}, {
	title: 'test',
	noiName: '',
	nodeMap: null,

	async testFn() {
		assert(false, 'Testing function is not defined');
	},

	run() {
		test(this.title, async () => {
			await this.testFn();
		});
	}
});

const TestSVG = Class(TestWrapper, null, {
	async testFn() {
		const diagram = await diagramBuilder(this.noiName, this.nodeMap, this.options);

		if (toRegenerateExpectations) {
			fs.writeFileSync(`${expectedDataFolder}/${this.title}.svg`, diagram);
			return;
		}

		const result = await getScreenshotData(diagram);

		const expectedSVG = fs.readFileSync(`${expectedDataFolder}/${this.title}.svg`, 'utf-8');
		const expected = await getScreenshotData(expectedSVG);
		const {width, height} = expected;

		assert.deepEqual([result.width, result.height], [width, height], 'Image dimentions must be equal');


		const diffImg = new PNG({
			width,
			height
		});
		let diff;
		try {
			diff = pixelmatch(expected.data, result.data, diffImg.data, width, height, {threshold: 0});
			if (diff > 0) {
				fs.writeFileSync(`${pkg.directories.testOutput}/${this.title}-actual.svg`, diagram);
				fs.writeFileSync(`${pkg.directories.testOutput}/${this.title}-actual.png`, PNG.sync.write(result));
				fs.writeFileSync(`${pkg.directories.testOutput}/${this.title}-diff.png`, PNG.sync.write(diffImg));
			}
		} catch (e) {
			// eslint-disable-next-line no-console
			console.warn(e);
			fs.writeFileSync(`${pkg.directories.testOutput}/${this.title}-actual.png`, PNG.sync.write(result));
			fs.writeFileSync(`${pkg.directories.testOutput}/${this.title}-base.png`, PNG.sync.write(expected));
		}

		assert.equal(diff, 0);
	}
});


suite('E2E', function() {
	suiteSetup(async () => {
		browser = await puppeteer.launch({
			/* eslint-disable spaced-comment */
			//headless: false,
			//devtools: true,
			/* eslint-enable spaced-comment */
			args: ['--no-sandbox']
		});
		page = await browser.newPage();
	});
	suiteTeardown(async () => {
		await page.close();
		await browser.close();
	});

	suite('single node', function() {
		[
			new TestSVG('without a link', 'Node', {
				Node: {}
			}),
			new TestSVG('with a link', 'Node', {
				Node: {
					link: '#Node'
				}
			})
		].forEach(function(testCase) {
			testCase.run();
		});
	});

	suite('parents', function() {
		[
			new TestSVG('node with one undocumented parent', 'Node', {
				Node: {
					parent: 'Parent'
				}
			}),
			new TestSVG('node with one documented parent', 'Node', {
				Node: {
					parent: 'Parent'
				},
				Parent: {
					link: '#Parent'
				}
			}),
			new TestSVG('node with one parent already converted into GraphNode', 'Node', {
				Node: {
					parent: new GraphNode('Parent', {link: '#Parent'}, {type: 'parent'})
				},
			}),
			...[
				[undefined],
				[null],
				[false],
				[true],
				[0],
				[1],
				[''],
				[{}, '{}'],
				[function() {}, 'function() {}'],
				[[], '[]']
			].map(([value, name]) => new TestSVG(`"${name || value}" parent must be skipped`, 'Node', {
				Node: {
					parent: value
				}
			})),
			new TestSVG('few parent levels', 'Node', {
				Node: {
					parent: 'Parent1'
				},
				Parent1: {
					parent: 'Parent2',
					link: '#Parent1'
				},
				Parent2: {
					parent: 'Parent3',
					link: '#Parent2'
				},
				Parent3: {
					parent: 'Parent4',
					link: '#Parent3'
				},
				Parent4: {
					parent: 'Parent5',
					link: '#Parent4'
				}
			})
		].forEach(function(testCase) {
			testCase.run();
		});
	});

	suite('children', function() {
		[
			new TestSVG('one undocumented child', 'Node', {
				Node: {
					children: ['Child']
				}
			}),
			new TestSVG('one documented child', 'Node', {
				Node: {
					children: ['Child']
				},
				Child: {
					link: '#Child'
				}
			}),
			new TestSVG('one child already converted into GraphNode', 'Node', {
				Node: {
					children: [
						new GraphNode('Child', {
							link: '#Child'
						}, {
							type: 'child'
						})
					]
				}
			}),
			new TestSVG('invalid children must be skipped', 'Node', {
				Node: {
					children: [...invalidSet, 'Valid']
				}
			}),
			new TestSVG('few different children', 'Node', {
				Node: {
					children: ['Child1', 'Child2', 'Child3', 'Child4', 'Child5']
				},
				Child1: {
					link: '#Child1'
				},
				Child2: {
					link: '#Child2'
				},
				Child4: {
					link: '#Child4'
				}
			})
		].forEach(function(testCase) {
			testCase.run();
		});
	});

	suite('position multiple levels of children', function() {
		[
			new TestSVG('two levels with one children', 'Node', {
				Node: {
					children: ['Child0']
				},
				Child0: {
					children: ['Child1']
				}
			}),
			new TestSVG('two levels with two children on the second level', 'Node', {
				Node: {
					children: ['Child0']
				},
				Child0: {
					children: ['Child10', 'Child11']
				}
			}),
			new TestSVG('two levels with three children on 1 and two children on the 2 level', 'Node', {
				Node: {
					children: ['Child00', 'Child01', 'Child02']
				},
				Child01: {
					children: ['Child10', 'Child11']
				}
			}),
			new TestSVG('three levels', 'Node', {
				Node: {
					children: ['Child00', 'Child01', 'Child02', 'Child03']
				},
				Child01: {
					children: ['Child10', 'Child11']
				},
				Child03: {
					children: ['Child13']
				},
				Child10: {
					children: ['Child20', 'Child21']
				}
			}),
			new TestSVG('shifting of the last top children', 'Node', {
				Node: {
					children: ['Child00', 'Child01', 'Child02']
				},
				Child00: {
					children: ['Child10', 'Child11']
				},
				Child02: {
					children: ['Child13']
				},
				Child10: {
					children: ['Child20', 'Child21', 'Child22']
				},
				Child11: {
					children: ['Child23', 'Child24']
				}
			})
		].forEach(function(testCase) {
			testCase.run();
		});
	});

	suite('mixins', function() {
		[
			new TestSVG('class with one undocumented mixin', 'Node', {
				Node: {
					mixes: ['Mixin']
				}
			}),
			new TestSVG('class with one documented mixin', 'Node', {
				Node: {
					mixes: ['Mixin']
				},
				Mixin: {
					link: '#Mixin'
				}
			}),
			new TestSVG('invalid mixins must be skipped', 'Node', {
				Node: {
					mixes: [...invalidSet, 'Valid']
				}
			}),
			new TestSVG('parent of a mixin is not displayed', 'Node', {
				Node: {
					mixes: ['Mixin']
				},
				Mixin: {
					link: '#Mixin',
					parent: 'ParentMixin'
				}
			}),
			new TestSVG('child mixins are not displayed', 'Node', {
				Node: {
					children: ['Child']
				},
				Child: {
					link: '#Child',
					mixes: ['Mixin']
				}
			}),
			new TestSVG('parent with one mixin', 'Node', {
				Node: {
					parent: 'Parent'
				},
				Parent: {
					link: '#Parent',
					mixes: ['Mixin']
				}
			}),
			new TestSVG('parent with two mixins', 'Node', {
				Node: {
					parent: 'Parent'
				},
				Parent: {
					parent: 'Object',
					link: '#Parent',
					mixes: ['Mixin', 'Mixin2']
				}
			}),
			new TestSVG('grand parent with few mixins', 'Node', {
				Node: {
					parent: 'Parent'
				},
				Parent: {
					parent: 'GrandParent',
					link: '#Parent'
				},
				GrandParent: {
					parent: 'Object',
					link: '#Parent',
					mixes: ['Mixin1', 'Mixin2']
				},
				Mixin1: {
					link: '#Mixin1'
				}
			}),
			new TestSVG('one mixin already converted into GraphNode', 'Node', {
				Node: {
					mixes: [
						new GraphNode('Mixin', {
							link: '#Mixin'
						}, {
							type: 'mixin'
						})
					]
				}
			}),
			new TestSVG('few different mixins', 'Node', {
				Node: {
					mixes: ['Mixin1', 'Mixin2', 'Mixin3', 'Mixin4', 'Mixin5']
				},
				Mixin1: {
					link: '#Mixin1'
				},
				Mixin2: {
					link: '#Mixin2'
				},
				Mixin4: {
					link: '#Mixin4'
				}
			})
		].forEach(function(testCase) {
			testCase.run();
		});
	});

	suite('class & interface', function() {
		[
			new TestSVG('class with one undocumented interface', 'Class', {
				Class: {
					implements: ['IInterface']
				}
			}),
			new TestSVG('class with one documented interface', 'Class', {
				Class: {
					implements: ['IInterface']
				},
				IInterface: {
					link: '#IInterface'
				}
			}),
			new TestSVG('invalid interfaces must be skipped', 'Node', {
				Node: {
					implements: [...invalidSet, 'Valid']
				}
			}),
			new TestSVG('parent of an interface is not displayed', 'Class', {
				Class: {
					implements: ['IInterface']
				},
				IInterface: {
					link: '#IInterface',
					parent: 'IParent'
				}
			}),
			new TestSVG('child interfaces are not displayed', 'Node', {
				Node: {
					children: ['Child']
				},
				Child: {
					link: '#Child',
					implements: ['IInterface']
				}
			}),
			new TestSVG('parent with one interface', 'Class', {
				Class: {
					parent: 'Parent'
				},
				Parent: {
					link: '#Parent',
					implements: ['IInterface']
				}
			}),
			new TestSVG('parent with few interfaces', 'Class', {
				Class: {
					parent: 'Parent'
				},
				Parent: {
					parent: 'Object',
					link: '#Parent',
					implements: ['IInterface1', 'IInterface2']
				},
				IInterface1: {
					link: '#IInterface1'
				}
			}),
			new TestSVG('grand parent with few interfaces', 'Class', {
				Class: {
					parent: 'Parent'
				},
				Parent: {
					parent: 'GrandParent',
					link: '#Parent'
				},
				GrandParent: {
					parent: 'Object',
					link: '#Parent',
					implements: ['IInterface1', 'IInterface2']
				},
				IInterface1: {
					link: '#IInterface1'
				}
			}),
			new TestSVG('one interface already converted into GraphNode', 'Class', {
				Class: {
					implements: [
						new GraphNode('Interface', {
							link: '#Interface'
						}, {
							type: 'interface'
						})
					]
				}
			}),
			new TestSVG('few different interfaces', 'Class', {
				Class: {
					implements: ['IInterface1', 'IInterface2', 'IInterface3', 'IInterface4', 'IInterface5']
				},
				IInterface1: {
					link: '#IInterface1'
				},
				IInterface2: {
					link: '#IInterface2'
				},
				IInterface4: {
					link: '#IInterface4'
				}
			})
		].forEach(function(testCase) {
			testCase.run();
		});
	});

	suite('complex class diagrams', function() {
		[
			new TestSVG('one undocumented parent, one child, one interface, one mixin', 'Class', {
				Class: {
					parent: 'Parent',
					children: ['Child'],
					implements: ['IInterface'],
					mixes: ['Mixin']
				}
			}),
			new TestSVG('one documented parent, child, interface and mixin', 'Class', {
				Class: {
					parent: 'Parent',
					children: ['DocChild'],
					implements: ['DocInterface'],
					mixes: ['DocMixin']
				},
				Parent: {
					link: '#DocParent'
				},
				DocChild: {
					link: '#DocChild'
				},
				DocInterface: {
					link: '#DocInterface'
				},
				DocMixin: {
					link: '#DocMixin'
				}
			}),
			new TestSVG('documented and undocumented parents, children, interfaces and mixins', 'Class', {
				Class: {
					parent: 'DocParent',
					children: ['DocChild', 'UndocChild'],
					implements: ['DocInterface', 'UndocInterface'],
					mixes: ['DocMixin', 'UndocMixin']
				},
				DocParent: {
					parent: 'UndocParent',
					link: '#Parent'
				},
				DocChild: {
					parent: 'Class',
					link: '#DocChild'
				},
				DocInterface: {
					link: '#DocInterface'
				},
				DocMixin: {
					link: '#DocMixin'
				}
			}),
			new TestSVG('few parents, few levels of children, few interfaces, few mixins', 'Class', {
				Class: {
					parent: 'Parent1',
					children: ['Child00', 'Child01', 'LooooooongChildName02', 'Child03'],
					implements: ['LooooooongInterfaceName0', 'IInterfaces1'],
					mixes: ['Mixin0', 'LooooooooongMixinName1', 'Mixin2']
				},
				Parent1: {
					parent: 'Parent0',
					children: ['Class', 'SomeOtherClass'],
					implements: ['IInterface2', 'IInterfaces3'],
					mixes: ['Mix1', 'Mix2']
				},
				Parent0: {
					parent: 'LooooooongParentName',
					link: '#Parent0'
				},
				Child01: {
					children: ['Child10', 'Child11'],
					link: '#Child01'
				},
				Child03: {
					children: ['Child13'],
					link: '#Child03'
				},
				Child10: {
					children: ['LooooooooongChildName20', 'Child21'],
					link: '#Child10'
				},
				Child13: {
					children: ['Child23', 'Child24'],
					link: '#Child13'
				},
				IInterface1: {
					parent: 'IParentInterface',
					link: '#IInterface1'
				}
			}),
			new TestSVG('complex diagram with nondefault properties', 'Class', {
				Class: {
					parent: 'Parent',
					children: ['Child', 'Child2'],
					implements: ['IInterface1', 'IInterface2'],
					mixes: ['Mixin', 'Mixin2', 'Mixin3']
				},
				Parent: {
					parent: 'Object',
					children: ['Node'],
					link: '#Parent'
				},
				Child: {
					link: '#Child'
				},
				Child2: {
					link: '#Child2',
					children: ['Child3', 'Child4']
				},
				Child4: {
					link: '#Child4'
				},
				IInterface2: {
					link: '#IInterface2'
				},
				Mixin3: {
					link: '#Mixin3'
				}
			}, {
				css: '.no-ref rect {fill: red;}',
				node: {
					padding: [20, 30],
					margin: [60, 30]
				},
				externalLinks: {
					Mixin: 'http://link.to/mixin/class.html'
				}
			})
		].forEach(function(testCase) {
			testCase.run();
		});
	});

	suite('XSS attacks (background is trying to be changed to red)', () => {
		[
			new TestSVG('XSS over the node name',
				'Node <script>document.querySelector("svg").style.background = "red";</script>',
				{
					'Node <script>document.querySelector("svg").style.background = "red";</script>': {}
				}
			),
			new TestSVG('XSS over the link', 'Node', {
				Node: {
					parent: 'Parent'
				},
				Parent: {
					link: '"><script>document.querySelector("svg").style.background = "red";</script></a><a title="'
				}
			}),
			new TestSVG('XSS over the css', 'Node', {
				Node: {}
			}, {
				css: '</style><script>document.querySelector("svg").style.background = "red";</script><style>',
			}),
			new TestSVG('XSS over the css bypassing CDATA in case it is used', 'Node', {
				Node: {}
			}, {
				css: ']]></style><script>document.querySelector("svg").style.background = "red";</script><style><![CDATA[',
			})
		].forEach(testCase => {
			testCase.run();
		});
	});
});
