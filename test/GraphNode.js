const assert = require('chai').assert;

const Module = require('../src/GraphNode');


suite('GraphNode (edge cases only)', () => {
	suite('Constructor', () => {
		test('must throw an error if name is not provided', () => {
			assert.throws(() => {
				new Module();
			}, TypeError, 'Invalid node name. It is expected a string with a length > 0');
		});

		test('must throw an error if name is an empty string', () => {
			assert.throws(() => {
				new Module('');
			}, TypeError, 'Invalid node name. It is expected a string with a length > 0');
		});

		test('an instance should be created without exceptions if additional properties are not provided', () => {
			assert.instanceOf(new Module('name'), Module);
		});
	});
});

