FROM node:latest
RUN apt-get update
# this libraries are needed to run headless browser for testing and code coverage
RUN apt-get install -y -q libx11-xcb1 libxcomposite1 libxcursor1 libxdamage1 libxi6 libxtst6 libnss3 libcups2 libxss1 libxrandr2 libasound2 libatk1.0-0 libatk-bridge2.0-0 libgtk-3-0


