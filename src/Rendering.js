/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/inheritance-diagram/blob/master/LICENSE.txt)
 * All source files are available at: https://gitlab.com/valerii-zinchenko/inheritance-diagram
 */


const {Class} = require('class-wrapper/dest/class-wrapper.amd');
const Parent = require('./ProcessingNode.js');

const path = require('path');
const puppeteer = require('puppeteer');
const GraphNode = require('../src/GraphNode.js');



/**
 * Rendering engine.
 *
 * It renders the positioned nodes and bind them with arrows.
 *
 * @class Rendering
 * @augments ProcessingNode
 */
module.exports = Class(Parent, null, /** @lends Rendering.prototype */ {
	__name: 'Rendering',

	/**
	 * Rendering properties.
	 *
	 * All number values are in pixels.
	 *
	 * @type {Object}
	 *
	 * @property {Object} node - Properties of a rendered node.
	 * @property {number[]} [node.margin = [30, 10]] - Default margins between nodes.
	 * @property {number[]} [node.padding = [10, 5]] - Default paddings for a text inside a node.
	 * @property {String} [css = ""] - Additional CSS for a diagram.
	 */
	properties: {
		node: {
			// eslint-disable-next-line no-magic-numbers
			margin: [30, 10],
			// eslint-disable-next-line no-magic-numbers
			padding: [10, 5]
		},
		css: ''
	},

	/**
	 * Main processing routine.
	 *
	 * @throws {TypeError} "noi" argument should be an instance of GraphNode
	 *
	 * @param {GraphNode} noi - Node of interest with resolved dependecies.
	 * @return {String} String of the whole SVG diagram.
	 */
	async process(noi) {
		if (!(noi instanceof GraphNode)) {
			throw new TypeError('"noi" argument should be an instance of GraphNode');
		}

		const browser = await puppeteer.launch({
			/* eslint-disable spaced-comment */
			//headless: false,
			//devtools: true,
			//dumpio: true,
			/* eslint-enable spaced-comment */
			args: ['--no-sandbox']
		});
		const page = await browser.newPage();
		const url = path.join(__dirname, 'rendering.html');
		await page.goto(`file:///${url}`);

		const result = await page.evaluate(
			(node, renderingProperties) => window.render(node, renderingProperties),
			noi,
			this.properties
		);

		await page.close();
		await browser.close();

		return result;
	}
});
