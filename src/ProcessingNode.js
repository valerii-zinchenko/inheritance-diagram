/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/inheritance-diagram/blob/master/LICENSE.txt)
 * All source files are available at: https://gitlab.com/valerii-zinchenko/inheritance-diagram
 */


const {Class, utils} = require('class-wrapper/dest/class-wrapper.amd');


/**
 * Abstract processing node
 *
 * @class ProcessingNode
 */
module.exports = Class(null, /** @lends ProcessingNode.prototype */ {
	__name: 'ProcessingNode ',

	/**
	 * Set of properties.
	 *
	 * @type {Object}
	 */
	properties: {},

	/**
	 * Reset properies.
	 *
	 * @param {Object} properties - Properties. Any of already defined properties can be redefined and new one can be added
	 */
	setProperties(properties) {
		utils.deepCopy(this.properties, properties);
	},

	/* c8 ignore next 6 */
	/**
	 * Main processing routine.
	 *
	 * @abstract
	 */
	async process() {}
});
