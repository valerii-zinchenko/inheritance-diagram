/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/inheritance-diagram/blob/master/LICENSE.txt)
 * All source files are available at: https://gitlab.com/valerii-zinchenko/inheritance-diagram
 */


const {Class} = require('class-wrapper/dest/class-wrapper.amd');
const Parent = require('./ProcessingNode.js');

const GraphNode = require('./GraphNode.js');


/**
 * Input adapter.
 *
 * It converts the raw objects into GraphNodes and prepares Node Of Interest (NOI) for futher processing.
 *
 * @class InputAdapter
 * @augments ProcessingNode
 */
module.exports = Class(Parent, null, /** @lends InputAdapter.prototype */{
	__name: 'InputAdapter ',

	/**
	 * Prepare raw NOI data for the further processing.
	 *
	 * It also collects and sets the parent stack for NOI.
	 *
	 * @throws {TypeError} "noiName" argument is expected to be a non-empty string
	 * @throws {TypeError} "map" argument is expected to be an instance of Object class
	 * @throws {Error} Node data for "${noiName}" does not exist in the provided node map
	 *
	 * @param {String} name - NOI name.
	 * @param {Object} map - Map of nodes.
	 * @returns {GrapchNode} - NOI.
	 */
	async process(name, map) {
		if (typeof name !== 'string' || name === '') {
			throw new TypeError('"name" argument is expected to be a non-empty string');
		}
		if (!(map instanceof Object)) {
			throw new TypeError('"map" argument is expected to be an instance of Object class');
		}

		const noiData = map[name];
		if (!noiData) {
			throw new Error(`Node data for "${name}" does not exist in the provided node map`);
		}

		const noi = new GraphNode(name, noiData, {
			type: 'noi',
			parentStack: this._prepareParentNodes(noiData.parent, map)
		});

		this._prepareNodes(noi.parentStack, map, 'parent');
		noi.parentStack.forEach(item => {
			this._prepareNodes(item.implements, map, 'interface');
			this._prepareNodes(item.mixes, map, 'mixin');
		});
		this._prepareNodesRecursivelyBy(noi.children, 'children', map, 'child');
		this._prepareNodes(noi.implements, map, 'interface');
		this._prepareNodes(noi.mixes, map, 'mixin');

		return noi;
	},

	/**
	 * Prepare parent node.
	 *
	 * The first node in stack is the actual parent node of the NOI. The last parent node is the root node of the inheritance chain.
	 * It also sets the correct type of the parent node.
	 *
	 * @private
	 *
	 * @param {String} name - Parent node name.
	 * @param {Object} map - Map of nodes.
	 * @returns {GraphNode[]} - Ordered stack of parent nodes.
	 */
	_prepareParentNodes(name, map) {
		let stack = [name];

		const node = map[name];
		if (node && node.parent) {
			stack = stack.concat(this._prepareParentNodes(node.parent, map));
		}

		return stack;
	},

	/**
	 * Prepare nodes recuresively.
	 *
	 * @private
	 *
	 * @param {Object[]} set - Set of node to process.
	 * @param {Object} map - Map of nodes.
	 * @param {string} type - Type of a node ('child', 'interface', 'mixin').
	 */
	_prepareNodesRecursivelyBy(set, setName, map, type) {
		this._prepareNodes(set, map, type);
		set.forEach(node => {
			this._prepareNodesRecursivelyBy(node[setName], setName, map, type);
		});
	},

	/**
	 * Prepare implemented side nodes (interfaces, mixins).
	 *
	 * @private
	 *
	 * @param {Object[]} set - Set of node to process.
	 * @param {Object} map - Map of nodes.
	 * @param {string} type - Type of a node ('interface', 'mixin').
	 */
	_prepareNodes(set, map, type) {
		set.forEach((name, index) => {
			// the nodes in the map can be already converted into GraphNode
			if (name instanceof GraphNode) {
				return;
			}

			// Replace node name with GraphNode
			set[index] = new GraphNode(name, map[name], {type});
		});
	}
});
