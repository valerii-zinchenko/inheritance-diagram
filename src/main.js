/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/inheritance-diagram/blob/master/LICENSE.txt)
 * All source files are available at: https://gitlab.com/valerii-zinchenko/inheritance-diagram
 */


const InputAdapter = require('./InputAdapter.js');
const Rendering = require('./Rendering.js');


/**
 * Diagram builder.
 *
 * @param {String} noiName - Name of a node for what the diagram should be built.
 * @param {Object} nodeMap - Map of nodes, where key is a node name and the value is an object of node properties.
 * @param {Object} [options] - Options for processing nodes: [InputAdapter]{@link InputAdapter#properties}, [Rendering]{@link Rendering#properties}. This object will be passed to each processor and each processor will take and use the needed options.
 */
module.exports = async function(noiName, nodeMap, options) {
	const inputAdapter = new InputAdapter();
	const rendering = new Rendering();

	if (options && options instanceof Object) {
		inputAdapter.setProperties(options);
		rendering.setProperties(options);
	}

	const noi = await inputAdapter.process(noiName, nodeMap);
	const result = await rendering.process(noi);

	return result;
};
