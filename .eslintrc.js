module.exports = {
	env: {
		es2020: true,
		browser: true,
		node: true,
		mocha: true
	},
	parserOptions: {
		sourceType: 'module'
	},
	extends: 'eslint:recommended',
	rules: {
		'no-console': 'warn',

		'default-case': 'error',
		'default-case-last': 'warn',
		'default-param-last': 'error',
		'dot-location': ['error', 'property'],
		'dot-notation': 'error',
		'eqeqeq': ['warn', 'always'],
		'no-alert': 'error',
		'no-else-return': 'warn',
		'no-eval': 'warn',
		'no-labels': 'warn',
		'no-loop-func': 'error',
		'no-magic-numbers': ['warn', {
			ignore: [-1, 0, 1, 2],
			ignoreArrayIndexes: true,
			enforceConst: true,
			detectObjects: true
		}],
		'no-multi-str': 'warn',
		'no-param-reassign': 'warn',
		'no-proto': 'error',
		'no-return-assign': ['error', 'always'],
		'no-return-await': 'warn',
		'no-script-url': 'warn',
		'no-self-compare': 'error',
		'no-sequences': 'error',
		'no-throw-literal': 'error',
		'no-unmodified-loop-condition': 'warn',
		'no-unused-expressions': ['warn', {
			"allowShortCircuit": true,
			"allowTernary": true
		}],
		'no-useless-call': 'error',
		'no-useless-concat': 'warn',
		'no-warning-comments': ['error', {
			terms: ['todo', 'fixme'],
			location: 'anywhere'
		}],
		'radix': ['error', 'always'],
		'yoda': ['error', 'never', {
			exceptRange: true
		}],

		'no-shadow': 'error',
		'no-undef-init': 'warn',
		'no-unused-vars': 'warn',
		'no-use-before-define': 'error',

		'array-bracket-spacing': ['warn', 'never'],
		'brace-style': ['warn', '1tbs'],
		'camelcase': 'warn',
		'func-call-spacing': ['error', 'never'],
		'func-style': ['warn', 'declaration'],
		'indent': ['warn', 'tab', {
			SwitchCase: 1,
			MemberExpression: 1
		}],
		'key-spacing': ['warn', {
			beforeColon: false,
			afterColon: true,
			mode: 'minimum'
		}],
		'keyword-spacing': ['warn', {
			before: true,
			after: true
		}],
		'max-depth': 'warn',
		'max-nested-callbacks': ['warn', 5],
		'max-params': ['warn', 5],
		'max-statements': ['warn', 50],
		'max-statements-per-line': ['error', {
			max: 1
		}],
		'new-cap': ['warn', {
			newIsCap: true,
			properties: true,
			capIsNewExceptions: ['Class']
		}],
		'new-parens': 'error',
		'newline-per-chained-call': ['error', {
			ignoreChainWithDepth: 2
		}],
		'no-lonely-if': 'warn',
		'no-mixed-operators': 'warn',
		'no-multiple-empty-lines': ['warn', {
			max: 3,
			maxEOF: 1
		}],
		'no-nested-ternary': 'error',
		'no-new-object': 'error',
		'no-trailing-spaces': 'warn',
		'no-unneeded-ternary': 'warn',
		'no-whitespace-before-property': 'error',
		'object-curly-spacing': ['warn', 'never'],
		'object-property-newline': 'warn',
		'one-var': ['warn', 'never'],
		'operator-assignment': ['warn', 'always'],
		'operator-linebreak': ['warn', 'before'],
		'padded-blocks': ['warn', 'never'],
		'padding-line-between-statements': 'warn',
		'quote-props': ['warn', 'as-needed'],
		'quotes': ['warn', 'single'],
		'require-jsdoc': 'warn',
		'semi': ['error', 'always'],
		'space-before-blocks': ['warn', 'always'],
		'space-before-function-paren': ['warn', {
			anonymous: 'never',
			named: 'never',
			asyncArrow: 'always'
		}],
		'space-in-parens': ['warn', 'never'],
		'space-infix-ops': 'warn',
		'spaced-comment': ['warn', 'always']
	}
};
