const fs = require('fs');
const builder = require('inheritance-diagram');

(async () => {
	const result = await builder(
		// Node for what the diagram will be built
		'Node',

		// Full map of nodes and relationships
		{
			Node: {
				parent: 'Parent',
				children: ['Child1', 'Child2', 'Child3'],
				implements: ['IInterface2', 'IInterface3'],
				mixes:['Mixin', 'Mixin2']
			},
			Parent: {
				parent: 'Object',
				children: ['Node'],
				implements: ['IInterface1'],
				mixes: ['MixinP1'],
				link: '#Parent'
			},
			Child1: {
				link: '#Child1',
			},
			Child2: {
				link: '#Child2',
			},
			Child3: {
				link: '#Child3',
				children: ['Child4', 'Child5']
			},
			Child4: {
				link: '#Child4',
			},
			IInterface1: {
				link: '#IInterface1'
			},
			IInterface2: {
				link: '#IInterface2'
			},
			Mixin2: {
				link: '#Mixin2'
			}
		}
	);

	fs.writeFileSync('out.svg', result);

	process.exit(0);
})();
