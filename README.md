# Inheritance diagram builder

This builds an inheritance diagram for some node of interest (NOI). Initially it is designed to use in documentations with class description to show parent stack, mixed in nodes and the whole tree of children.

Nodes that contains `link` property will be highlighted and will behave as usual link element to allow fast jumping to the next interested node in the documentation.

Mainly this library was created for JSDoc (the JSDoc plugin is [`jsdoc-inheritance-diagram`](https://www.npmjs.com/package/jsdoc-inheritance-diagram)), but it can be used as a core for other purposes.


## Limitations and known issues

1. Arrows at the end of connection line cannot inherit the line styles (colors).

	* [SVG v2](https://www.w3.org/TR/2016/CR-SVG2-20160915/Overview.html) will fix this by defining the context: [Specifying paint](https://www.w3.org/TR/2018/CR-SVG2-20181004/painting.html#TermContextElement)

Due to upcomming features/improvements in [SVG v2](https://www.w3.org/TR/2016/CR-SVG2-20160915/Overview.html) the above limitations will not have coded workarounds.

Hope [SVG v2](https://www.w3.org/TR/2016/CR-SVG2-20160915/Overview.html) will be released and implemented by browsers soon, or maybe some other idea comes to me later :)


## Links

* [API](http://valerii-zinchenko.gitlab.io/inheritance-diagram/)


## Exmaple

The example can be found in [`example.js`](https://gitlab.com/valerii-zinchenko/inheritance-diagram/-/blob/master/example.js) file.

```
npm install inheritance-diagram
node example.js
```

If CSS from an external file must be applied to the diagram, then an additional code is needed for this, for example:

```js
const diagramBuilder = require('inheritance-diagram');

require('fs').readFile('styles.css', 'utf8', async (err, data) => {
	if (err) {
		throw err;
	}

	const result = await diagramBuilder('inheritance-diagram'))('...', {...}, {
		css: data
	});
});
```

### Result

![Example of an inheritance diagram](./example.png)
